import java.util.ArrayList;
import java.util.List;

/**
 * Created by peizheng on 2015/2/6.
 */
public class Folder extends FileObject {

	private List<FileObject> childs = new ArrayList<FileObject>();

	public Folder(String name, String path, Folder parent)
	{
		this.name = name;
		this.path = path;
		this.parent = parent;
	}

	public Folder(String name, String path)
	{
		this.name = name;
		this.path = path;
	}

	public void mkdir(String child)
	{
		if(child == null || child.isEmpty())
			return;
		int slash = child.indexOf('/');
		if(slash < 0)
			childs.add(new Folder(child, this.path + "/" + child));
		else {
			String childName = child.substring(0, slash);
			Folder newChild = new Folder(childName, this.path + "/" + childName, this);
			childs.add(newChild);
			newChild.mkdir(child.substring(slash + 1));
		}
	}

	public String ls()
	{
		StringBuilder sb = new StringBuilder();
		for(FileObject child : childs) {
			if(child instanceof Folder)
				sb.append("/");
			sb.append(child.name + "\n");
		}
		return sb.toString();
	}

	public Folder cd(String path)
	{
		if(path == null || path.isEmpty()) {
			return this;
		}
		if("..".equals(path))
			return this.parent;
		int slash = path.indexOf('/');
		if(slash < 0)
			slash = path.length();
		for(FileObject child : this.childs) {
			if(child instanceof Folder && child.name.equals(path.substring(0, slash))) {
				return ((Folder) child).cd(path.substring(slash));
			}
		}
		return null;
	}

	public void touch(String name)
	{
		File newChild = new File(name, this.path + "/" + name, this);
		this.childs.add(newChild);
	}
}
