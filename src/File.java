/**
 * Created by peizheng on 2015/2/6.
 */
public class File extends FileObject {

	private String content = null;

	public File(String name, String path, Folder parent)
	{
		this.name = name;
		this.path = path;
		this.parent = parent;
	}
}
