/**
 * Created by peizheng on 2015/2/6.
 */
public class FileObject {

	protected String name = null;
	protected String path = null;
	protected Folder parent = null;

	public FileObject(String name, String path, Folder parent)
	{
		this.name = name;
		this.path = path;
		this.parent = parent;
	}

	public  FileObject()
	{

	}
}
