/**
 * Created by peizheng on 2015/2/6.
 */
public class FileSystem {

	Folder root = new Folder("", "", null);

	public void mkdir(String absPath)
	{
		String path = absPath.substring(1);
		root.mkdir(path);
	}

	public String cd(String path)
	{
		String rst = null;
		Folder newRoot = root.cd(path);
		if(newRoot == root)
			rst = root.path;
		root = newRoot;
		return rst;
	}

	public String ls()
	{
		return this.root.ls();
	}

	public void touch(String name)
	{
		this.root.touch(name);
	}


	public static void main(String[] args)
	{
		FileSystem fs = new FileSystem();

		fs.mkdir("/aa/bb");
		fs.mkdir("/AA/BB");
		assert "/aa\n/AA\n".equals(fs.ls());

		fs.cd("aa");
		fs.touch("cc");
		assert "/bb\ncc\n".equals(fs.ls());

		fs.cd("..");
		assert "/aa\n/AA\n".equals(fs.ls());

	}
}
